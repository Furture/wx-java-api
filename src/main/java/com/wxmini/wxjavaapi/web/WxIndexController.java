package com.wxmini.wxjavaapi.web;

import com.wxmini.wxjavaapi.utils.ResponseUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/wx/index")
public class WxIndexController {
    private final Log logger = LogFactory.getLog(WxIndexController.class);

    @RequestMapping
    public Object index(){
        return ResponseUtil.ok("hello world, this is wx service");
    }
}
